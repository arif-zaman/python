# Arif Uz Zaman // CSE,BUET // 1005031

while True:
    sum = 0
    n1 = int (raw_input("Enter Number1 : "))
    n2 = int (raw_input("Enter Number2 : "))
    
    if n1==0 & n2==0:
        break
    
    if n1>n2:
        print "Input Number in Correct Order."
        continue
    
    else:
        if (n1%2==0 & n2%2==0):
            for i in range(n1,n2+1,2):
                sum += i
                print sum-i,'+',i,' = ',sum
                
        elif (n1%2!=0 & n2%2==0):
            for i in range(n1+1,n2+1,2):
                sum += i
                print sum-i,'+',i,' = ',sum
        
        elif (n1%2==0 & n2%2!=0):
            for i in range(n1,n2,2):
                sum += i
                print sum-i,'+',i,' = ',sum
                
        elif (n1%2!=0 & n2%2!=0):
            for i in range(n1+1,n2,2):
                sum += i
                print sum-i,'+',i,' = ',sum
    
    print "Summation of All Even Numbers Between ",n1,' and ',n2,' = ',sum
            
