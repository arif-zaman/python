# Arif Uz Zaman // CSE,BUET // 1005031
# Series 3**1 to 3**power then (3**power - 2*3**i-1)

def printResult(p):
    level = 2*p
    for i in range(level,0,-1):
        str1 = ''
        str1 = str1 + (i-1)*' '
        if i>p:
            for j in range(1,level+2-i):
                str1 = str1 + str(3**j)+' '
        else:
            maxValue = 3**p
            x = p-i+2
            for k in range(1,p+1):
                str1 = str1 + str(3**k)+' '
            for l in range(1,x):
                maxValue = maxValue - 2*(3**(l-1))
                str1 = str1 + str(maxValue)+' '
                
        print str1
        
while True:
    power = int(raw_input('Enter Power of 3 :'))
    if power<1:
        break
    
    printResult(power)