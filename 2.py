# Arif Uz Zaman // CSE,BUET // 1005031

num = []

def get_num():
    a = input("Enter Number 1:" )
    b = input("Enter Number 2:" )
    num.insert(0,a)
    num.insert(1,b)

def add():
    result = num[0]+num[1]
    print num[0],'+',num[1],' = ',result

def sub():
    result = num[0]-num[1]
    print num[0],'-',num[1],' = ',result

def mul():
    result = num[0]*num[1]
    print num[0],'*',num[1],' = ',result

def div():
    if (num[1] != 0):
        result = num[0]/num[1]
        print num[0],'/',num[1],' = ',result
    else:
        print "Nope, Can't Divide by Zero.Input Correctly!"
        
def menu():
    while True:
        print "\n*****************Menu************************"
        print "Choose among the following options:"
        print "1. Press + to add."
        print "2. Press - to sub."
        print "3. Press * to multiply"
        print "4. Press / to divide."
        print "5. Press $ to exit.\n"
        
        res = raw_input("Enter Your Choice: ")
        
        if (res== '+'):
            get_num()
            add()
        
        elif (res== '-'):
            get_num()
            sub()
        
        elif (res== '*'):
            get_num()
            mul()
        
        elif (res== '/'):
            get_num()
            div()
            
        elif (res== '$'):
            break
            
        else:
            print "Please, Choose a Valid Option."

menu()