# Arif Uz Zaman // CSE,BUET // 1005031

def removeBrakets(list1):
    str1 = str(list1)
    return str1.replace('[','').replace(',','').replace(']','')

def findN(m): # Find How Many Level/Row
    sum = 0
    i=1
    while sum<=m:
        sum = sum+i
        if sum == m:
            break     
        i=i+1
    
    if sum==m:    
        return i
    else:
        return 0
        

def style(m):
    n = findN(m)
    if n==0:
        print "\nNope, Please input Correctly!\n"
        exit()
    result = []
    for i in range(1,m+1):
        result.append(i)
            
    #print result
    a= 0
    for i in range(1,n+1):
        b = a+n+1-i
        print removeBrakets(result[a:b])
        a = b
        
while True:   
    m = int(raw_input("\nEnter Number(Number Must be Sum of first n Numbers\n(like- 1,3,6,10,15,21....)) : "))
    if m<1:
        break
    style(m)
